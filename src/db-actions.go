package main

import (
    "fmt"
    "strconv"
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
)

func createConnectedDB() *sql.DB {
	conf := getConfig()
	// Create Connection
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8", conf.DBConfig.User, conf.DBConfig.Pass, conf.DBConfig.Host, conf.DBConfig.Port, conf.DBConfig.Name))
    if err != nil {
		panic(err)
	}
    return db
}

func getSiteSaltByUserFromDB(user string, site string) string {
    var site_salt string
    db := createConnectedDB()
    // query site_code by user and site domain
	rows, err := db.Query(fmt.Sprintf("SELECT site_salt FROM site_code WHERE user_id='%s' AND site_domain='%s'", user, site))
	if err != nil {
		panic(err)
	}
	if rows.Next() {
		err = rows.Scan(&site_salt)
		if err != nil {
			panic(err)
		}
	}
	db.Close()
    return site_salt
}

func insertSiteSaltToDB(user string, site string) string {
    site_salt := newSalt()
    db := createConnectedDB()
    
    // insert a new record in site_code table
	stmt, err := db.Prepare("INSERT site_code SET user_id=?, site_domain=?, site_salt=?")
	if err != nil { panic(err) }
	res, err := stmt.Exec(user, site, site_salt)
	if err != nil { panic(err) }
	_, err = res.LastInsertId()
	if err != nil { panic(err) }

	db.Close()
    return strconv.Itoa(site_salt)
}