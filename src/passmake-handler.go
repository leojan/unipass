package main

import (
    "fmt"
    "net/http"
    "strings"
    "time"
    "math/rand"
    "crypto/md5"
	"encoding/hex"
)

func makeUnipass(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    fmt.Println("user request:", r.URL.Path)
    user := strings.Join(r.Form["user"], "")
    pass := strings.Join(r.Form["pass"], "")
    site := strings.Join(r.Form["site"], "")

    if isInputInvalid(user, pass, site) {
        http.Error(w, "bad request", http.StatusBadRequest)
        return
    }

    salt := getSiteSaltByUserFromDB(user, site)

    if salt == "" { 
        fmt.Println("salt not found! create and insert to db.")
        salt = insertSiteSaltToDB(user, site)
    }
    
    fmt.Fprintf(w, hashString(pass, salt)) // send password to client side
}

func isInputInvalid(user string, pass string, site string) bool {
    return (len(user) < 4) || (len(pass) < 4) || (len(site) < 4)
}

func hashString(pass string, salt string) string {
    passAndSalt := pass + salt
    md5HashInBytes := md5.Sum([]byte(passAndSalt))
	md5HashInString := hex.EncodeToString(md5HashInBytes[:])
    return md5HashInString
}

func newSalt() int {
    s := rand.NewSource(time.Now().UnixNano())
    r := rand.New(s)
    return r.Intn(100)
}

// func checkErr(err error) {
// 	if err != nil {
// 		panic(err)
// 	}
// }